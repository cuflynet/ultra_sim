/****************************************************************************

Simulation of ultrasonic rangefinder

Nodes:
subscribed pose and quat from Vicon (geometry_msgs::TransformStamped)
published  pose and quat to MAVROS (geometry_msgs::PoseStamped)
both in 

****************************************************************************/

#include <ros/ros.h>
#include <geometry_msgs/Vector3.h>
#include <geometry_msgs/Quaternion.h>
#include <geometry_msgs/PoseStamped.h>
#include <geometry_msgs/Pose.h>
#include <sensor_msgs/LaserScan.h>
#include <tf/transform_datatypes.h>
#include <tf/LinearMath/Matrix3x3.h>
#include <tf/LinearMath/Quaternion.h>
#include <gazebo_msgs/ModelStates.h>
#include <math.h>
#include <pthread.h>
#define PI 3.14159265

// p-thread calls
pthread_mutex_t topicBell;
pthread_cond_t newPose;

// Global Vars
int loop_hz = 100;
int gaz_count = 0;
float max_val = 0.05;
int range_val = 4096;
// Gazebo Pose
geometry_msgs::Pose gaz_pos;
geometry_msgs::PoseStamped gaz_pos_stamp;
geometry_msgs::PoseStamped loc_pos;
// Ultrasonics
// Call back Function for Grabbing Vicon Info 
/*void gaz_Callback(const gazebo_msgs::ModelStates& msg)
{
  ++gaz_count;
  gaz_pos = msg.pose[1];
  pthread_cond_signal(&newPose);
}*/

void posCallback(const geometry_msgs::PoseStamped& msg)
{
  loc_pos = msg;
}

double yaw_q2e(double qz, double qw)
{
	double yaw = 2.0*asin(qz);
	return yaw;
}

float rand_gen(float max_val, int range)
{
	float r1 = (float)(rand() % range);
	r1 = r1-((float)range/2.0);
	r1 = (float)r1*max_val/range;
	return r1;
}

int main(int argc, char **argv)
{ 
  //======= ROS Setup ================
  ros::init(argc, argv, "ultra_sim_node");
  ros::NodeHandle n; 
  //======= ROS Publishers ================
  ros::Publisher ultra_publisher = n.advertise<sensor_msgs::LaserScan>("ultrasonics_range", 1);
  //======= ROS Subscribers ===============
  // Ros setup async spinners for subscibers
  ros::AsyncSpinner spinner(2);    
  // ROS setup subscriber
  //ros::Subscriber gaz_subscriber = n.subscribe("gaz", 1, gaz_Callback);
    
    ros::Subscriber sub_pos = n.subscribe("pose", 1, posCallback);
  // Start the Spinner
  spinner.start();

   // Publisher Loop
  ros::Rate loop_rate(loop_hz);
  
  topicBell = PTHREAD_MUTEX_INITIALIZER;
  pthread_cond_init(&newPose, NULL);
  
  while (ros::ok())
  {
    double x_pos = loc_pos.pose.position.x;
    double y_pos = loc_pos.pose.position.y;
    double z_pos = loc_pos.pose.position.z;
    double g_pos = yaw_q2e(loc_pos.pose.orientation.z,loc_pos.pose.orientation.w);
    ROS_INFO("X:%f, Y:%f, Z:%f",x_pos,y_pos,z_pos);
    sensor_msgs::LaserScan range_msg;
    range_msg.angle_min = 0;
    range_msg.angle_max = 2*PI;
    range_msg.angle_increment = 2*PI/5;
    range_msg.time_increment = 0.0;
    range_msg.scan_time = 1/20.0;
    range_msg.range_min = 0.0;
    range_msg.range_max = 7.0;
    range_msg.ranges.resize(5);
    range_msg.ranges = {0,0,0,0,0};
    range_msg.header.seq = gaz_count;
    range_msg.header.stamp = ros::Time::now();
    range_msg.header.frame_id = "fcu";

    // Ultrasonic message is foward, right, back, left, down
    // Assuming zero yaw
    float front,right,back,left,altitude;
    if(x_pos > 3)
    {
	front = 0;
    }
    else
    {
    	front = 3 - x_pos;
    }

    if(x_pos < -3)
    {
	back = 0;
    }
    else
    {
    	back = 3 + x_pos;
    }

    if(y_pos > 3)
    {
	left = 0;
    }
    else
    {
    	left = 3 - y_pos; 
    }

    if(y_pos < -3)
    {
	right = 0;
    }
    else
    {
    	right = 3 + y_pos;
    }
    altitude = z_pos + rand_gen(max_val,range_val);
    back = -1;
    range_msg.ranges = {front,right,back,left,altitude};

    ultra_publisher.publish(range_msg);
    // Spin Once:
    ros::spinOnce();
	// Maintain loop rate
    loop_rate.sleep();
  }
  return 0;
}




